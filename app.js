const http = require('http');
const fetch = require('cross-fetch')
const axios = require('axios')
const hostname = '127.0.0.1';
const port = 3000;
const nfts=[{
    project:'stoned_ape_crew',
    quantity:3,
},
{
    project:'nuked_apes',
    quantity:2,
},
{
    project:'sea_shanties',
    quantity:4,
},
{
    project:'degods',
    quantity:1,
},
{
    project:'bat_city_underground',
    quantity:2,
},
{
    project:'billionaire_bens',
    quantity:2,
},
{
    project:'cat_cartel',
    quantity:2,
},
{
    project:'bounty_hunter_space_guild',
    quantity:1,
},
{
    project:'high_roller_hippo_clique',
    quantity:2,
},
{
    project:'anybodies',
    quantity:1,
},
{
    project:'the_geisha_clan',
    quantity:2,
},
{
    project:'creatures',
    quantity:2,
},
{
    project:'anybodies_fashion',
    quantity:1,
},
{
    project:'bigbrainbrothers',
    quantity:1,
},
{
    project:'block_chumps',
    quantity:1,
},
{
    project:'blockieverse',
    quantity:2,
},
{
    project:'chinesepunkz',
    quantity:1,
},
{
    project:'citizens_by_solsteads',
    quantity:1,
},
{
    project:'cryptograf',
    quantity:2,
},
{
    project:'defi_pirates',
    quantity:2,
},
{
    project:'ice_cream_parlour_monkes',
    quantity:4,
},
{
    project:'panthers_in_the_metaverse',
    quantity:2,
},
{
    project:'saiba_gang',
    quantity:1,
},
{
    project:'scumbag',
    quantity:1,
},
{
    project:'solairdroppass',
    quantity:1,
},
{
    project:'solana_diamond_boys',
    quantity:3,
},
{
    project:'solana_money_boys',
    quantity:2,
},
{
    project:'solana_monkey_university',
    quantity:5,
},
{
    project:'solthirsty_apes',
    quantity:10,
},
{
    project:'solthirsty_drunkapes',
    quantity:5,
},
{
    project:'solthirsty_jug_of_beer',
    quantity:2,
},
{
    project:'desolates_metaverse',
    quantity:1
},
{
    project: 'sea_shanties_citizens',
    quantity:4
}
]

function getUrl(collection){
    return 'https://api-mainnet.magiceden.dev/v2/collections/'+ collection + '/stats';
}

async function getFloorPrice(collection){
    try{
        
    const url = getUrl(collection);
    console.log(url)
    const res = await fetch(url);
  const data = await res.json();//assuming data is json
//   .then(data => {return data});
   return data.floorPrice/1000000000
   
    }catch (error) {
       
          console.log(error);
      }
}
const server = http.createServer(async(req, res) => {
    try{
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  let totalFloor = 0;
//   for await (const nft of nfts) {
//     const floor =  await getFloorPrice(nft.project);
//     console.log(totalFloor + floor*nft.quantity)
//     totalFloor = totalFloor + floor*nft.quantity;
//   }
  await Promise.all(
    nfts.map(async(nft)=>{
       const floor =  await getFloorPrice(nft.project);
       totalFloor = totalFloor + floor*nft.quantity;
       console.log(nft.project)
       console.log(floor*nft.quantity )
       return '';
    })
  )

    console.log(totalFloor)
    res.end(totalFloor.toString());
   }catch (error) {
    console.log(error);
    }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});